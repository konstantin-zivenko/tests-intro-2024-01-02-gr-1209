import unittest
from tricky_revers import tricky_revers
from parameterized import parameterized


class TestTrickyRevers(unittest.TestCase):
    @parameterized.expand(
        [("", ""), ("abcd efgh", "dcba hgfe"), ("a1bcd efg!h", "d1cba hgf!e")]
    )
    def test_typical(self, arg, result):
        self.assertEqual(tricky_revers(arg), result)

    @parameterized.expand(
        [(None,), (12,)],
    )
    def test_atypical(self, arg):
        self.assertRaises(TypeError, tricky_revers, arg)


if __name__ == "__main__":
    unittest.main()
