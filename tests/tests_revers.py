import pytest

from tricky_revers import tricky_revers


@pytest.mark.parametrize(
    "argument, result",
    (
        ("", ""),
        ("abcd efgh", "dcba hgfe"),
        ("a1bcd efg!h", "d1cba hgf!e"),
    ),
)
def test_typical_cases(argument, result):
    assert tricky_revers(argument) == result


cases = (None, True, [], (1, 2, 3), 13, 0.1234)


@pytest.mark.parametrize("argument", cases)
def test_atypical_cases(argument):
    with pytest.raises(TypeError) as err:
        tricky_revers(argument)
        print(err)


if __name__ == "__main__":
    pytest.main()
