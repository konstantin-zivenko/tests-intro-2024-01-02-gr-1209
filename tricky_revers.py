def get_words(text: str) -> list[str]:
    return text.split()


def invert_word_alpha(word: str) -> str:
    """Get reversed word (alpha symbols - revers, non-alpha symbols - stay on old places).

    Usage examples:
    >>> invert_word_alpha('abcd')
    'dcba'
    >>> invert_word_alpha('a1bcd')
    'd1cba'
    >>> invert_word_alpha(12345)
    Traceback (most recent call last):
    ...
    TypeError: invalid data type for invert_word_alpha(). Expected 'str', but got <class 'int'>
    """
    if not isinstance(word, str):
        raise TypeError(
            f"invalid data type for invert_word_alpha(). Expected 'str', but got {type(word)}"
        )

    inverted_word = ""
    letters = [
        letter for letter in word if letter.isalpha()
    ]  # "a1bcd" -> ["a", "b", "c", "d"]
    for symbol in word:
        inverted_word += letters.pop() if symbol.isalpha() else symbol
    return inverted_word


def tricky_revers(text: str) -> str:
    if not isinstance(text, str):
        raise TypeError(
            f"invalid data type for tricky_revers(). Expected 'str', but got {type(text)}"
        )
    result = []
    for word in get_words(text):
        result.append(invert_word_alpha(word))
    return " ".join(result)


if __name__ == "__main__":
    cases = (("", ""), ("abcd efgh", "dcba hgfe"), ("a1bcd efg!h", "d1cba hgf!e"))
    for arg, result in cases:
        assert (
            tricky_revers(arg) == result
        ), f"expected: tricky_revers('{arg}') = '{result}', but got '{tricky_revers(arg)}'"
